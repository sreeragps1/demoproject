import React from "react";
import { Routes,Route,NavLink,BrowserRouter as Router} from 'react-router-dom';
import { AdminPortal } from "./AdminPortal";
import { App } from "./App";
import { ClientPortal } from "./ClientPortal";
import './header.scss';
//import { Home } from "./Home";

export const Header = () => { 
      
  return (
    <div className="header">
      <div className="header__content">
      <div className="header__content__title">
        <h3>
         React_App
        </h3>
      </div>
     
   
         <Router>
         <div className="header__content__primaryNavigation">
        <NavLink className = 'header__content__primaryNavigation__items'   to='/'   activestyle={{color:'blue'}}>Portals</NavLink>
        <NavLink className = 'header__content__primaryNavigation__items'   to='/adminPortal'   activestyle={{color:'blue'}}>AdminPortal</NavLink>
        <NavLink className = 'header__content__primaryNavigation__items'   to='/clientPortal'  activestyle={{color:'blue'}}>ClientPortal</NavLink>
        </div>
       </Router>
       
      
      </div>
    </div>
  )
}


//export default App;
