import React from "react";
import './app.scss';
import { Header } from "./Header";
import { Home } from "./Home";
import { Routes,Route,NavLink,BrowserRouter } from 'react-router-dom';
import { AdminPortal } from "./AdminPortal";
import { ClientPortal } from "./ClientPortal";

export const App = () => {
 
      
  return (
    <div className="app">
        <Header />
       <BrowserRouter>
        
          <Routes>
            <Route  path="/"  element={<Home />}/>
            <Route  path="/adminPortal"  element={<AdminPortal />}/>
            <Route  path="/clientPortal"  element={<ClientPortal />} />
        </Routes>
      </BrowserRouter>

          
    </div>
  )
}


//export default App;
